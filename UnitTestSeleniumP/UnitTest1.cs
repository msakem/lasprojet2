﻿
using System;
using System.Windows.Forms;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.IE;

namespace UnitTestSeleniumP
{
 
    [TestFixture]
    public class UntitledTestCase
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            //driver = new FirefoxDriver();
            //baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();
            driver = new InternetExplorerDriver(@"D:\Msakem\driver\IEDriverServer_Win32_3.5.1\");

        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            NUnit.Framework.Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test()]
        public void TheUntitledTestCaseTest()
        {
            driver.Navigate().GoToUrl("http://www.google.fr");
            driver.FindElement(By.Name("q")).SendKeys("rien");

            // lance la recherche
            driver.FindElement(By.Name("q")).Submit();

            // le test réussit si on trouve un lien dont le texte est Rien - Wikipédia

            bool res = true;

            try
            {
                driver.FindElement(By.LinkText("Rien - Wikip&eacute;dia"));
            }
            catch
            {
                res = false;
            }
            NUnit.Framework.Assert.IsTrue(true);
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
    
  
}
